#!/usr/bin/python

import cgi, cgitb
cgitb.enable()

print("Content-Type:text/html\n\n")
print('''
<html>
	<title>Test</title>
	<head>
		<style>
			<link rel="stylesheet" type="text/css" href="https://www.cs.mcgill.ca/~szheng14/cgi-bin/testcss.css">
		</style>
	</head>
	<body>
		<h1>h1</h1>
		<h2>h2</h2>
		<h3>h3</h3>
		<img src="death.png" alt="death" width=500 height=200>
		<div id="test">
			<p>Hello</p>
			<li>
				abc
			</li>
		</div>
		<div class="flexbox">
			<form name="input" action="transporter.py" method="POST">
				<input type="hidden" name="URL" value="https://www.cs.mcgill.ca/~szheng14/">
				<center>
					<input type="radio" name="inventory" value="11,10" checked> 10 manna, 10 gold
					<input type="radio" name="inventory" value="6,20"> 5 manna, 20 gold
					<input type="radio" name="inventory" value="16,5"> 15 manna, 5 gold
					<br></br>
					<center><input type="text" id="comment" name="comment"></center>
					<input type="submit" value="Depart" name="submit">
				</center>
			</form>
		</div>
	</body>
</html>
''')

#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgi

def main(args):
	print('''Content-Type:text/html;charset=utf-8

<!DOCTYPE html>
<html lang="en">
<head>
<title>Transferring to cgi-bin</title>
<style>
@-webkit-keyframes blink {
    0% {
        opacity: 1;
    }
    50% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
}
@-moz-keyframes blink {
    0% {
        opacity: 1;
    }
    50% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
}
@-o-keyframes blink {
    0% {
        opacity: 1;
    }
    50% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
}
img {
	opacity: 0.5;
    filter: alpha(opacity=50);
    -webkit-animation: blink 1s;
    -webkit-animation-iteration-count: infinite;
    -moz-animation: blink 1s;
    -moz-animation-iteration-count: infinite;
    -o-animation: blink 1s;
    -o-animation-iteration-count: infinite;
}
body {
	background-size: cover;
	background-repeat: no-repeat;
	padding-top: 40px;
}
h1 {
		color: #1CA51E;
}
h2 {
		color: #D24015;
}
body {
	background-image: url(https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-271486.jpg);
}
</style>
</head>
<body>
<center><h1>Sending you to cgi-bin!</h1></center>
<center><h2>Please wait a moment...</h2></center>
<center><img src="red-alert.png" alt="RED ALERT" width="150" height="150"></center>
<p></p><p></p>
<center> &nbsp &nbsp &nbsp &nbsp <img src="http://vignette4.wikia.nocookie.net/custombionicle/images/5/53/Loading_bar.gif/revision/20150122161333" alt="Loading..." width="940" height="120"></center>
<meta http-equiv="refresh" content="3;url=https://www.cs.mcgill.ca/~szheng14/cgi-bin/">
</body>
</html>
''')
	
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))

#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgi

def main(args):
	
	gotoURL = cgi.FieldStorage().getvalue("gotoURL","/~szheng14/")
	
	if all(prefix not in gotoURL for prefix in ('~szheng14','http')): gotoURL = 'https://' + gotoURL
	
	print('''Content-Type:text/html;charset=utf-8

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Let's go!</title>
		<style>
			@-webkit-keyframes blink {
				0%% {
					opacity: 1;
				}
				25%% {
					opacity: 1;
				}
				50%% {
					opacity: 0;
				}
				75%% {
					opacity: 1;
				}
				100%% {
					opacity: 1;
				}
			}
			@-moz-keyframes blink {
				0%% {
					opacity: 1;
				}
				25%% {
					opacity: 1;
				}
				50%% {
					opacity: 0;
				}
				75%% {
					opacity: 1;
				}
				100%% {
					opacity: 1;
				}
			}
			@-o-keyframes blink {
				0%% {
					opacity: 1;
				}
				25%% {
					opacity: 1;
				}
				50%% {
					opacity: 0;
				}
				75%% {
					opacity: 1;
				}
				100%% {
					opacity: 1;
				}
			}
			.centerflash {
				opacity: 0.5;
				filter: alpha(opacity=50);
				-webkit-animation: blink 3s;
				-webkit-animation-iteration-count: infinite;
				-moz-animation: blink 3s;
				-moz-animation-iteration-count: infinite;
				-o-animation: blink 3s;
				-o-animation-iteration-count: infinite;
				background: url("https://media.giphy.com/media/10HCdQ6QWEO0hO/giphy.gif") no-repeat center;
				background-size: cover;
			}
			body {
				position: absolute;
				width: 50%%;
				height: 50%%;
				transform: translateX(50%%) translateY(10%%);
			}
			html {
				background: url("https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-205313.jpg") no-repeat center fixed;
				background-size: cover;
			}
			h1 {
				color: #D11100;
			}
			form {
				opacity: 0.5;
				filter: alpha(opacity=50);
				-webkit-animation: blink 5s;
				-webkit-animation-iteration-count: infinite;
				-moz-animation: blink 5s;
				-moz-animation-iteration-count: infinite;
				-o-animation: blink 5s;
				-o-animation-iteration-count: infinite;
			}
		</style>
	</head>
	
	<body>
		<div class="centerflash">
			<center>
				<h1>You have successfully transported to the linked page!</h1>
				<h1>--------------------> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <--------------------</h1>
				<h1>Press the button to activate thrusters and cross the portal!</h1>
			</center>
		</div>
		
		<center>
			<p>*</p>
			<p>*</p>
			<p>*</p>
			<p>*</p>
		</center>
		<center>
			<form action="%s">
				<input type="submit" value="LET'S GOOOO">
			</form>
		<center>
	</body>
</html>
''' % (gotoURL))
	
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
